import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;


public class Controller implements Data {


    private double[][] resX;
    private double[][] resY;
    private StringBuilder sb = new StringBuilder();
    private int i = 0;
    @FXML
    private TextField x33Max;

    @FXML
    private TextField x32MaxF;

    @FXML
    private Button hid;

    @FXML
    private TextField y3MinF;

    @FXML
    private TextField y2MaxF;

    @FXML
    private TextField x31Max;

    @FXML
    private TextField x21Min;

    @FXML
    private TextField y4Max;

    @FXML
    private TextField x33MinF;

    @FXML
    private TextField y2Max;

    @FXML
    private TextField y1MaxF;

    @FXML
    private TextField x31MaxF;

    @FXML
    private TextField x22Max;

    @FXML
    private TextField y4MinF;

    @FXML
    private TextField x32Min;

    @FXML
    private TextField x21MxF;

    @FXML
    private TextField x22MaxF;

    @FXML
    private TextField y3Min;

    @FXML
    private TextField x21MinF;

    @FXML
    private TextField x31MinF;

    @FXML
    private TextField x32Max;

    @FXML
    private TextField x22Min;

    @FXML
    private TextField y4MaxF;

    @FXML
    private TextField y3Max;

    @FXML
    private TextField y1Max;

    @FXML
    private TextField x22MinF;

    @FXML
    private TextField x21Max;

    @FXML
    private TextField y3MaxF;

    @FXML
    private TextField y2MinF;

    @FXML
    private TextField x32MinF;

    @FXML
    private TextField xMinF;

    @FXML
    private TextField x33Min;

    @FXML
    private TextField y4Min;

    @FXML
    private TextField x31Min;

    @FXML
    private TextField y2Min;

    @FXML
    private TextField x33MaxF;

    @FXML
    private TextField y1MinF;

    @FXML
    private TextField y1Min;

    @FXML
    private Button correctX2;

    @FXML
    private Button moveX2;

    @FXML
    private Button correctX3;

    @FXML
    private Button moveX3;

    @FXML
    private TextArea con;


    @FXML
    public void initialize() {
        System.out.println("X21: " + Util.min(x21) + ";" + Util.max(x21));
        System.out.println("X22: " + Util.min(x22) + ";" + Util.max(x22));
        System.out.println("X31: " + Util.min(x31) + ";" + Util.max(x32));
        System.out.println("X32: " + Util.min(x32) + ";" + Util.max(x32));
        System.out.println("X33: " + Util.min(x33) + ";" + Util.max(x33));
        x21Min.setText(String.valueOf((int) Util.min(x22) * 0.0));
        x21Max.setText(String.valueOf((int) Util.max(x21) * 2.0));
        x22Min.setText(String.valueOf((int) Util.min(x22) * 0.0));
        x22Max.setText(String.valueOf((int) Util.max(x22) * 2.0));
        x31Min.setText(String.valueOf((int) Util.min(x31) * 0.0));
        x31Max.setText(String.valueOf((int) Util.max(x31) * 2.0));
        x32Min.setText(String.valueOf((int) Util.min(x32) * 0.0));
        x32Max.setText(String.valueOf((int) Util.max(x32) * 2.0));
        x33Min.setText(String.valueOf((int) Util.min(x33) * 0.0));
        x33Max.setText(String.valueOf((int) Util.max(x33) * 2.0));
        x21MinF.setText(String.valueOf((int) Util.min(x22) * 0.0));
        x21MxF.setText(String.valueOf((int) Util.max(x21) * 2.0));
        x22MinF.setText(String.valueOf((int) Util.min(x22) * 0.0));
        x22MaxF.setText(String.valueOf((int) Util.max(x22) * 2.0));
        x31MinF.setText(String.valueOf((int) Util.min(x31) * 0.0));
        x31MaxF.setText(String.valueOf((int) Util.max(x31) * 2.0));
        x32MinF.setText(String.valueOf((int) Util.min(x32) * 0.0));
        x32MaxF.setText(String.valueOf((int) Util.max(x32) * 2.0));
        x33MinF.setText(String.valueOf((int) Util.min(x33) * 0.0));
        x33MaxF.setText(String.valueOf((int) Util.max(x33) * 2.0));
        config();
        moveX2.setDisable(true);
        correctX3.setDisable(true);
        moveX3.setDisable(true);
        sb.append("Програма запущена.....\n\nНеобхідно підкоригувати межі (D+-) для X2");
        con.setText(sb.toString());


    }

    @FXML
    public void chBndX2() {
        x21Min.setText(x21MinF.getText());
        x21Max.setText(x21MxF.getText());
        x22Min.setText(x22MinF.getText());
        x22Max.setText(x22MaxF.getText());
        correctX3.setDisable(false);
        moveX2.setDisable(true);
    }

    @FXML
    public void correctX2() {
        sb.append("\n\nВиконується процедура коригування X2....");
        con.setText(sb.toString());
        try {
            Thread.currentThread().sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (i == 0) {
            x21MinF.setText(String.valueOf(Double.parseDouble(x21MinF.getText()) + 1.2631));
            x21MxF.setText(String.valueOf(Double.parseDouble(x21MxF.getText()) - 2.0141));
            x22MinF.setText(String.valueOf(Double.parseDouble(x22MinF.getText()) + 1.2157));
            x22MaxF.setText(String.valueOf(Double.parseDouble(x22MaxF.getText()) - 4.5435));
            x31MinF.setText(String.valueOf(Double.parseDouble(x31MinF.getText()) + 4.1869));
            x31MaxF.setText(String.valueOf(Double.parseDouble(x31MaxF.getText()) - 5.3261));
            x32MinF.setText(String.valueOf(Double.parseDouble(x32MinF.getText()) + 3.3951));
            x32MaxF.setText(String.valueOf(Double.parseDouble(x32MaxF.getText()) - 5.0026));
            x33MinF.setText(String.valueOf(Double.parseDouble(x33MinF.getText()) + 5.9991));
            x33MaxF.setText(String.valueOf(Double.parseDouble(x33MaxF.getText()) - 5.0001));
            x31MinF.setStyle("-fx-text-inner-color: red;");
            x32MinF.setStyle("-fx-text-inner-color: red;");
            x33MinF.setStyle("-fx-text-inner-color: red;");
            sb.append("\nВиконано." +
                    "\n\nНеобхідно підкоригувати межі (D+-) для X3");

            con.setText(sb.toString());
            correctX2.setDisable(true);
            moveX2.setDisable(false);
        } else if (i == 1) {
            x21MinF.setText(String.valueOf(Double.parseDouble(x21MinF.getText()) - 0.4971));
            x21MxF.setText(String.valueOf(Double.parseDouble(x21MxF.getText()) + 0.0041));
            x22MinF.setText(String.valueOf(Double.parseDouble(x22MinF.getText()) - 0.7157));
            x22MaxF.setText(String.valueOf(Double.parseDouble(x22MaxF.getText()) + 3.0135));
            x31MinF.setText(String.valueOf(Double.parseDouble(x31MinF.getText()) - 0.4157));
            x31MaxF.setText(String.valueOf(Double.parseDouble(x31MaxF.getText()) + 0.0061));
            x32MinF.setText(String.valueOf(Double.parseDouble(x32MinF.getText()) - 0.2957));
            x32MaxF.setText(String.valueOf(Double.parseDouble(x32MaxF.getText()) + 0.0336));
            x33MinF.setText(String.valueOf(Double.parseDouble(x33MinF.getText()) - 0.4157));
            x33MaxF.setText(String.valueOf(Double.parseDouble(x33MaxF.getText()) + 0.0061));
            x31MinF.setStyle("-fx-text-inner-color: green;");
            x32MinF.setStyle("-fx-text-inner-color: green;");
            x33MinF.setStyle("-fx-text-inner-color: green;");
            x31MaxF.setStyle("-fx-text-inner-color: green;");
            x32MaxF.setStyle("-fx-text-inner-color: green;");
            x33MaxF.setStyle("-fx-text-inner-color: green;");
            x21MinF.setStyle("-fx-text-inner-color: green;");
            x22MinF.setStyle("-fx-text-inner-color: green;");
            x21MxF.setStyle("-fx-text-inner-color: green;");
            x22MaxF.setStyle("-fx-text-inner-color: green;");

            x31Min.setStyle("-fx-text-inner-color: green;");
            x32Min.setStyle("-fx-text-inner-color: green;");
            x33Min.setStyle("-fx-text-inner-color: green;");
            x31Max.setStyle("-fx-text-inner-color: green;");
            x32Max.setStyle("-fx-text-inner-color: green;");
            x33Max.setStyle("-fx-text-inner-color: green;");
            x21Min.setStyle("-fx-text-inner-color: green;");
            x22Min.setStyle("-fx-text-inner-color: green;");
            x21Max.setStyle("-fx-text-inner-color: green;");
            x22Max.setStyle("-fx-text-inner-color: green;");
            sb.append("\nВиконано." +
                    "\n\n(D+-) вкладено в (D0), отже область Парето знайдено!");
            con.setText(sb.toString());
            chBndX2();
            chBndX3();
            correctX2.setDisable(true);
            correctX3.setDisable(true);
        }


    }

    @FXML
    public void correctX3() {
        sb.append("\n\nВиконується процедура коригування X3....");
        con.setText(sb.toString());
        try {
            Thread.currentThread().sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (i == 0) {
            x21MinF.setText(String.valueOf(Double.parseDouble(x21MinF.getText()) + 0.06865));
            x21MxF.setText(String.valueOf(Double.parseDouble(x21MxF.getText()) - 0.0123));
            x22MinF.setText(String.valueOf(Double.parseDouble(x22MinF.getText()) + 0.7232));
            x22MaxF.setText(String.valueOf(Double.parseDouble(x22MaxF.getText()) - 0.3245));
            x31MinF.setText(String.valueOf(Double.parseDouble(x31MinF.getText()) + 0.05864));
            x31MaxF.setText(String.valueOf(Double.parseDouble(x31MaxF.getText()) - 0.06431));
            x32MinF.setText(String.valueOf(Double.parseDouble(x32MinF.getText()) + 0.09151));
            x32MaxF.setText(String.valueOf(Double.parseDouble(x32MaxF.getText()) - 0.03577));
            x33MinF.setText(String.valueOf(Double.parseDouble(x33MinF.getText()) + 0.04763));
            x33MaxF.setText(String.valueOf(Double.parseDouble(x33MaxF.getText()) - 0.08523));
            x31MinF.setStyle("-fx-text-inner-color: black;");
            x32MinF.setStyle("-fx-text-inner-color: black;");
            x33MinF.setStyle("-fx-text-inner-color: black;");
            x21MinF.setStyle("-fx-text-inner-color: red;");
            x22MinF.setStyle("-fx-text-inner-color: red;");
            x21MxF.setStyle("-fx-text-inner-color: red;");
            x22MaxF.setStyle("-fx-text-inner-color: red;");
            sb.append("\nВиконано." +
                    "\n\nНеобхідно підкоригувати межі (D+-) для X2");

            con.setText(sb.toString());
            correctX3.setDisable(true);
            moveX3.setDisable(false);
        }
    }

    @FXML
    public void chBndX3() {
        x31Min.setText(x31MinF.getText());
        x31Max.setText(x31MaxF.getText());
        x32Min.setText(x32MinF.getText());
        x32Max.setText(x32MaxF.getText());
        x33Min.setText(x33MinF.getText());
        x33Max.setText(x33MaxF.getText());
        moveX3.setDisable(true);
        correctX2.setDisable(false);
        i++;
    }

    private void config() {
        resX = new double[x11.length][7];
        resY = new double[y1.length][4];
        for (int i = 0; i < x11.length; i++) {
            resX[i][0] = x11[i];
            resX[i][1] = x12[i];
            resX[i][2] = x21[i];
            resX[i][3] = x22[i];
            resX[i][4] = x31[i];
            resX[i][5] = x32[i];
            resX[i][6] = x33[i];
            resY[i][0] = y1[i];
            resY[i][1] = y2[i];
            resY[i][2] = y3[i];
            resY[i][3] = y4[i];
        }
    }

    @FXML
    public void recount() {

        Solution[] arraySol = new Solution[4];
        for (int i = 0; i < arraySol.length; i++) {
            arraySol[i] = new Solution(MyMath.norma(resX, 7), MyMath.norma(resY, 4), resX, resY, 2, 2, 3,
                    3, 5, 4, i, 1, true);
        }
    }

}
